#lang plai-typed
;; Tipos
(define-type ExprC
  [numC  (n    : number)]
  [idC   (s    : symbol)]
  [appC  (fun  : ExprC)  (arg  : ExprC)]
  [plusC (l    : ExprC)  (r    : ExprC)]
  [multC (l    : ExprC)  (r    : ExprC)]
  [lamC  (arg  : symbol) (body : ExprC)]
  [ifC   (c    : ExprC)  (s    : ExprC)  (n    : ExprC)]
  )

(define-type Value
  [numV (n : number)]
  [closV (arg : symbol) (body : ExprC) (env : Env)]
  )

(define-type ExprS
  [numS    (n : number)]
  [idS     (s : symbol)]
  [appS    (fun : ExprS)  (arg : ExprS)]
  [lamS    (arg : symbol) (body : ExprS)]
  [plusS   (l : ExprS)     (r   : ExprS)]
  [bminusS (l : ExprS)     (r   : ExprS)]
  [uminusS (e : ExprS)]
  [multS   (l : ExprS)     (r   : ExprS)]
  [ifS     (c : ExprS)     (s   : ExprS)  (n : ExprS)]
  )

(define-type Binding
  [bind (name : symbol) (val : Value)]
  )

(define-type-alias Env (listof Binding))

(define mt-env empty)
(define extend-env cons)

(define (num+ [l : Value] [r : Value]) : Value
  (cond
   [(and (numV? l) (numV? r))
    (numV (+ (numV-n l) (numV-n r)))]
   [else
    (error 'num "Um dos argumentos não é número")]
   ))

(define (num* [l : Value] [r : Value]) : Value
  (cond
   [(and (numV? l) (numV? r))
    (numV (* (numV-n l) (numV-n r)))]
   [else
    (error 'num "Um dos argumentos não é número")]
   ))

(define (parse [s : s-expression]) : ExprS
  (cond
   [(s-exp-number? s) (numS (s-exp->number s))]
   [(s-exp-symbol? s) (idS  (s-exp->symbol s))]
   [(s-exp-list? s)
    (let ([sl (s-exp->list s)])
      (case (s-exp->symbol (first sl))
        [(+)    (plusS   (parse (second sl))         (parse (third sl)))]
        [(*)    (multS   (parse (second sl))         (parse (third sl)))]
        [(-)    (bminusS (parse (second sl))         (parse (third sl)))]
        [(~)    (uminusS (parse (second sl)))]
        [(call) (appS    (parse (second sl)) (parse (third sl)))]
        [(func) (lamS    (s-exp->symbol (second sl))
                         (parse (third sl)))]
        [(if)   (ifS     (parse (second sl))
                         (parse (third sl))
                         (parse (fourth sl)))]
        [else   (error 'parse "invalid list input")]
        ))]
   [else (error 'parse "invalid list input")]))

(define (desugar [as : ExprS]) : ExprC
  (type-case ExprS as
             [numS    (n)       (numC  n)]
             [idS     (s)       (idC   s)]
             [lamS     (a b)   (lamC  a           (desugar b))]
             [appS    (fun arg) (appC  (desugar fun) (desugar arg))]
             [plusS   (l r)     (plusC (desugar l) (desugar r))]
             [multS   (l r)     (multC (desugar l) (desugar r))]
             [bminusS (l r)     (plusC (desugar l) (multC (numC -1) (desugar r)))]
             [uminusS (e)       (multC (numC -1)   (desugar e))]
             [ifS     (c s n)   (ifC   (desugar c) (desugar s) (desugar n))]
             ))

(define (lookup [for : symbol] [env : Env]) : Value
  (cond
   [(empty? env) (error 'lookup "name not found")]
   [else
    (cond
     [(symbol=? for (bind-name (first env))) (bind-val (first env))]
     [else (lookup for (rest env))]
     )]
   ))

(define (interp [a : ExprC]
                [env : Env]) : Value
  (type-case ExprC a
             [numC  (n) (numV n)]
             [idC   (n) (lookup n env)]
             [lamC  (a b) (closV a b env)]
             [appC  (f a)
                    (local ([define f-value (interp f env)])
                           (interp (closV-body f-value)
                                   (extend-env
                                    (bind (closV-arg f-value) (interp a env))
                                    (closV-env f-value)
                                    )))]
             [plusC (l r)   (num+ (interp l env) (interp r env))]
             [multC (l r)   (num* (interp l env) (interp r env))]
             [ifC   (c s n) (if (zero? (numV-n (interp c env))) (interp n env) (interp s env))]
             ))

(define a
  '(func f x (+ x 2)))

(define (interpS [s : s-expression]) (interp (desugar (parse s)) mt-env))

;; (test (interp (plusC (numC 10) (appC (fdC 'const5 '_ (numC 5)) (numC 10)))
;;               mt-env)
;;       (numV 15))
;; (interpS '(+ 10 (call (func dobra x (+ x x)) 16)))

